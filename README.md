# Unibly Test #

This is a test application I implemented as a test when I applied to Unibly.

# Requirement #
1. Splash Screen
The app contains a splash screen that will show for 500 ms before enter the main page.
2. Main Page
In the main page, show a list of data from the following API.
https://randomuser.me/api/?page=1&results=10&seed=android
In each row of the list, show the user's thumbnail and name.
When the user scroll to the end of the list, load and show the next 10 results.
(You may query the next page by increment the "page" value in the API)
3. Details page
When click on an item in main page's list, go to the details page.
Show the large icon, gender, email and phone of the user.

Note,
Just present the data in the way you think is good.
You may use any library or tool that you think is good.